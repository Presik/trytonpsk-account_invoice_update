# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pyson import Eval


class InvoiceUpdateStart(ModelView):
    'Invoice Update Start'
    __name__ = 'account_invoice.update.start'
    date = fields.Date('Date')
    description = fields.Char('Description')
    tax_add = fields.Many2One('account.tax', 'Add Tax', domain=[
        ('group.kind', '=', Eval('group_tax'))
        ], depends=['group_tax'])
    tax_remove = fields.Many2One('account.tax', 'Remove Tax', domain=[
        ('group.kind', '=', Eval('group_tax'))
        ], depends=['group_tax'])
    group_tax = fields.Char('Group Tax')

    @staticmethod
    def default_group_tax():
        Invoice = Pool().get('account.invoice')
        invoice_ids = Transaction().context['active_ids']
        invoice = Invoice(invoice_ids[0])
        if invoice.type == 'in':
            return 'purchase'
        else:
            return 'sale'


class InvoiceUpdate(Wizard):
    'Invoice Update'
    __name__ = 'account_invoice.update'
    start = StateView('account_invoice.update.start',
        'account_invoice_update.invoice_update_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        Invoice = Pool().get('account.invoice')
        Line = Pool().get('account.invoice.line')
        invoices = Invoice.browse(Transaction().context['active_ids'])

        values = {}
        if self.start.date:
            values['invoice_date'] = self.start.date
        if self.start.description:
            values['description'] = self.start.description

        invoices = [i for i in invoices if i.state == 'draft']
        if values:
            Invoice.write(invoices, values)

        if (self.start.tax_add or self.start.tax_remove) and invoices:
            invoice = invoices[0]
            lines_to_change = []
            for line in invoice.lines:
                if line.type != 'line':
                    continue
                lines_to_change.append(line)

            if lines_to_change:
                if self.start.tax_add:
                    Line.write(lines_to_change, {'taxes': [
                        ('add', [self.start.tax_add.id])]})
                if self.start.tax_remove:
                    Line.write(lines_to_change, {'taxes': [
                        ('remove', [self.start.tax_remove.id])]})
                    print('tax remove')
                if self.start.tax_add or self.start.tax_remove:
                    invoice.write([invoice], {'taxes': []})
                    invoice.on_change_taxes()
                    # Invoice.update_taxes([invoice])
                    print('tax remove fact')
                for tax in invoice.taxes:
                    print(tax.account.name, tax.amount)
            # invoice.save()
        return 'end'
