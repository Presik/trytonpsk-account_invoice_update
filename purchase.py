# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pyson import Eval


class PurchaseUpdateStart(ModelView):
    'Purchase Update Start'
    __name__ = 'purchase.update.start'
    date = fields.Date('Date')
    description = fields.Char('Description')
    tax_add = fields.Many2One('account.tax', 'Add Tax', domain=[
        ('group.kind', '=', Eval('group_tax'))
        ], depends=['group_tax'])
    tax_remove = fields.Many2One('account.tax', 'Remove Tax', domain=[
        ('group.kind', '=', Eval('group_tax'))
        ], depends=['group_tax'])
    group_tax = fields.Char('Group Tax')

    @staticmethod
    def default_group_tax():
        return 'purchase'


class PurchaseUpdate(Wizard):
    'Purchase Update'
    __name__ = 'purchase.update'
    start = StateView('purchase.update.start',
        'account_invoice_update.purchase_update_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def _update_all_dates(self):
        pass

    def transition_accept(self):
        Purchase = Pool().get('purchase.purchase')
        Line = Pool().get('purchase.line')
        purchases = Purchase.browse(Transaction().context['active_ids'])

        values = {}
        if self.start.date:
            values['purchase_date'] = self.start.date
        if self.start.description:
            values['description'] = self.start.description

        purchases = [p for p in purchases if p.state == 'draft']
        if values:
            Purchase.write(purchases, values)

        if (self.start.tax_add or self.start.tax_remove) and purchases:
            purchase = purchases[0]
            lines_to_change = []
            for line in purchase.lines:
                if line.type != 'line':
                    continue
                lines_to_change.append(line)

            if lines_to_change:
                if self.start.tax_add:
                    Line.write(lines_to_change, {'taxes': [
                        ('add', [self.start.tax_add.id])]})
                if self.start.tax_remove:
                    Line.write(lines_to_change, {'taxes': [
                        ('remove', [self.start.tax_remove.id])]})
            purchase.save()
        return 'end'
